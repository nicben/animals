﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Animals
{
    public abstract class Animal
    {
        public string Name { get; set; }
        public double Weight { get; set; }
        public enum Biome
        {
            ocean, 
            grasslands
        }

        public abstract void Present();

        public abstract void MakeNoise();

        public virtual void Sleep()
        {
            Console.WriteLine("I'm sleeping");
        }

    }
}

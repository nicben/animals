﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Animals
{
    class BabyShark : Shark
    {
        public BabyShark(string name, double length) : base(name, length)
        {
            Name = name;
            Lenght = length;
        }

        public override void Present()
        {
            Console.WriteLine($"My name is  {this.Name} and i'm a {this.GetType().Name} and i live in the {Animal.Biome.ocean}");
        }
        public override void MakeNoise()
        {
            Console.WriteLine("Baby shark, do do, do do do do");
        }

        public override void Sleep()
        {
            Console.WriteLine("I'm sleeping while swimming around");
        }
    }
}

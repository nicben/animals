﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Animals
{
    public interface IClimber
    {
        public void Climb();
    }
}

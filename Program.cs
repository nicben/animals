﻿using System;

namespace Animals
{
    class Program
    {
        static void Main(string[] args)
        {
            Lion lion = new Lion("Coco");

            lion.Present();
            lion.Pray();
            lion.MakeNoise();
            lion.Climb();
            lion.Sleep();

            Shark shark = new Shark("Hai", 400.0);

            shark.Present();
            shark.Pray();
            shark.MakeNoise();
            shark.Sleep();

            BabyShark babyshark = new BabyShark("Baby", 100.0);

            babyshark.Present();
            babyshark.Pray();
            babyshark.MakeNoise();
            babyshark.Sleep();
        }
    }
}

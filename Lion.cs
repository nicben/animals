﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Animals
{
    public class Lion : Animal, IPreditor, IClimber
    {
        public Lion(string name)
        {
            Name = name;
        }

        public override void Present()
        {
            Console.WriteLine($"My name is {this.Name} and i'm a {this.GetType().Name} and i live in the {Animal.Biome.ocean}");
        }

        public override void MakeNoise()
        {
            Console.WriteLine("ROAAAAR");
        }
        public override void Sleep()
        {
            Console.WriteLine("I'm resting and sleeping all day long");
        }

        public void Pray()
        {
            Console.WriteLine("I hunt at night");
        }

        public void Climb()
        {
            Console.WriteLine("I'm a climber");
        }
    }
}

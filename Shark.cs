﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Animals
{
    public class Shark : Animal, IPreditor
    {
        public Shark(string name, double length)
        {
            Name = name;
            Lenght = length;
        }

        public double Lenght { get; set; }

        public override void Present()
        {
            Console.WriteLine($"My name is {this.Name} and i'm a {this.GetType().Name} and i live in the {Animal.Biome.ocean}");
        }
        public override void MakeNoise()
        {
            Console.WriteLine("Blub blub blub");
        }

        public override void Sleep()
        {
            Console.WriteLine("I'm sleeping while swimming around");
        }
        public void Pray()
        {
            Console.WriteLine("I swim after my meals");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Animals
{
    public interface IPreditor
    {
        public void Pray();
    }
}
